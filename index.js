// Loggedin status of user
user_status = {
    login: false,
    role: false,
    name: false,
    filteruser: []
}

function loginSuccess(email){
        user_status.login = true;
        user_status.name = data[email].username;
        user_status.role = data[email].role;
        document.getElementById('login-status').innerHTML = `Name: ${user_status.name} Role: ${user_status.role}`;
        if (user_status.role == 'admin') {
            user_status.filteruser = ['admin', 'operations', 'sales']
        } else if (user_status.role == 'operations') {
            user_status.filteruser = ['operations', 'sales']
        } else {
            user_status.filteruser = ['sales']
        }
        document.getElementById('view-users').style.display = 'block';
        document.getElementById('view-register').style.display = 'none';
        document.getElementById('view-login').style.display = 'none';
        document.getElementById('btn-logout').style.display = 'block';
        getUser()
        document.getElementById("collapseThree").classList.add("show");
}

function login() {
    let email = document.getElementById('l-email').value;
    let pwd = document.getElementById('l-pwd').value;
    let check = email in data ? true : false;
    if (check && data[email].password == pwd) {
        loginSuccess(email)
        alert('Logged In Successfully')
    } else {
        alert('Incorrect Credentials Please Try Again')
    }
    return false;
}

function register() {
    let email = document.getElementById('s-email').value;
    let pwd = document.getElementById('s-pwd').value;
    let username = document.getElementById('s-user').value;
    let firstname = document.getElementById('s-fn').value;
    let lastname = document.getElementById('s-ln').value;
    let gender = document.querySelector('input[name="gender"]:checked').value;
    let role = document.querySelector('input[name="role"]:checked').value;

    data[email] = {
        email: email,
        password: pwd,
        username: username,
        firstname: firstname,
        lastname: lastname,
        gender: gender,
        role: role
    };
    alert('Registered Successfully');

    document.getElementById('register').reset();
    loginSuccess(email)

    return false;
}

// Get All Users according to role
function getUser() {
    let str = `<table class="table">
    <thead class="thead-dark">
        <tr>
            <th scope="col">#</th>
            <th scope="col">Email</th>
            <th scope="col">Role</th>
            <th scope="col">More Info</th>
        </tr>
    </thead>
    <tbody>`;
    let i = 1;
    Object.keys(data).map(d => {
        
        if (user_status.filteruser.includes(data[d].role)){
            let clr;
            if(data[d].role == 'admin'){
                clr = 'bg-danger'
            }else if(data[d].role == 'operations'){
                clr = 'bg-warning'
            }else{
                clr = 'bg-success'
            }
            str += ` <tr class=${clr}>
        <th scope="row">${i}</th>
        <td>${data[d].email}</td>
        <td>${data[d].role}</td>
        <td>  
        <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#example${data[d].username}">
           More Information
          </button></td>
          <div class="modal fade" id="example${data[d].username}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">${data[d].username}</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
        email: ${data[d].email} <br>
        username: ${data[d].username} <br>
        firstname: ${data[d].firstname} <br>
        lastname: ${data[d].lastname} <br>
        gender: ${data[d].gender} <br>
        role: ${data[d].role}
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
    </tr>
    `
    i++;
    }
    })
    str += `</tbody>
</table>
`;
    document.getElementById('show-users').innerHTML = str;
}

function logout() {
    user_status = {
        login: false,
        role: false,
        name: false,
        filteruser: []
    }
    document.getElementById('view-users').style.display = 'none';
    document.getElementById('view-register').style.display = 'block';
    document.getElementById('view-login').style.display = 'block';
    document.getElementById('btn-logout').style.display = 'none';
    document.getElementById('login-status').innerHTML = 'Login To View Users';

}

