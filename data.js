data = []
// Sample Data of Users
data['abc@gmail.com'] = {
    email: 'abc@gmail.com',
    password: 'abc',
    username: 'abc',
    firstname: 'AB',
    lastname: 'CD',
    gender: 'male',
    role: 'admin'
}
data['xyz@gmail.com'] = {
    email: 'xyz@gmail.com',
    password: 'xyz',
    username: 'xyz',
    firstname: 'XY',
    lastname: 'YZ',
    gender: 'female',
    role: 'operations'
}
data['ajay@gmail.com'] = {
    email: 'ajay@gmail.com',
    password: 'ajay',
    username: 'ajay',
    firstname: 'XY',
    lastname: 'YZ',
    gender: 'male',
    role: 'sales'
}
data['chaman@gmail.com'] = {
    email: 'chaman@gmail.com',
    password: 'chaman',
    username: 'chaman',
    firstname: 'CHAMAN',
    lastname: 'WAMAN',
    gender: 'male',
    role: 'operations'
}
